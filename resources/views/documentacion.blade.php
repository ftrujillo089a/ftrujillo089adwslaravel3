<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Documentación</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
		<link href="../public/css/estilos.css" rel="stylesheet" type="text/css">
        </head>
    <body>
              <div class="content">
                 <div class="title">GESTIÓN PROVINCIAS Y POBLACIONES</div>
                <div class="links">
				<p>
                    <a href="documentacion/DWST11LARAVELPROYECTO.pdf">Documentación Tema Laravel</a></p>
				<p>
					<a href="documentacion/proyecto1.pdf">Documentación Proyecto</a></p>
				<p>
					<a href="documentacion/DESPLIEGUE.pdf">Despliegue de la Aplicación</a></p>
                    
                </div>
            </div>
       
    </body>
</html>
