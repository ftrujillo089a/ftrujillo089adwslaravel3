

@extends('poblaciones')

@section('content')

<a class="btn btn-success pull-right" href="{{ url('/poblaciones/create') }}" role="button">Nueva población</a>

@include('poblaciones.partials.table')

@endsection

