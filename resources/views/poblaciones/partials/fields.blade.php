
<div class="form-group">
    {!! Form::label('codigo', 'Código', ['for' => 'codigo'] ) !!}
    {!! Form::text('codigo', null , ['class' => 'form-control', 'codigo' => 'codigo', 'placeholder' => 'Escribe el código ...' ]  ) !!}
	{!! Form::label('codigo_provincia', 'Código Provincia', ['for' => 'codigo_provincia'] ) !!}
    {!! Form::text('codigo_provincia', null , ['class' => 'form-control', 'codigo_provincia' => 'codigo_provincia', 'placeholder' => 'Escribe el código provincia ...' ]  ) !!}

    {!! Form::label('nominacion', 'Nominación', ['for' => 'nominacion'] ) !!}
    {!! Form::text('nominacion', null , ['class' => 'form-control', 'nominacion' => 'nominacion', 'placeholder' => 'Escribe el nombre de la población...' ]  ) !!}
    {!! Form::label('superficie', 'Superficie', ['for' => 'superficie'] ) !!}
    {!! Form::text('superficie', null , ['class' => 'form-control', 'superficie' => 'superficie', 'placeholder' => 'Escribe la superficie...' ]  ) !!}
    
	{!! Form::label('habitantes', 'Habitantes', ['for' => 'habitantes'] ) !!}
    {!! Form::text('habitantes', null , ['class' => 'form-control', 'habitantes' => 'habitantes', 'placeholder' => 'Escribe los habitantes...' ]  ) !!}
    {!! Form::label('gobierno', 'Gobierno', ['for' => 'gobierno'] ) !!}
    {!! Form::text('gobierno', null , ['class' => 'form-control', 'gobierno' => 'gobierno', 'placeholder' => 'Escribe el gobierno...' ]  ) !!}

</div>


