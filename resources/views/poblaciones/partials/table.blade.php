<h1 class="text-primary">Control de Poblaciones</h1>

<table class="table table-bordered" id="MyTable">
    <thead>
        <tr>
            <th class="text-center">Código</th>
			<th class="text-center">Nominación</th>
			<th class="text-center">Código provincia</th>
			<th class="text-center">Provincia</th>
           	<th class="text-center">Superficie</th>
			<th class="text-center">Habitantes</th>
			<th class="text-center">Gobierno</th>
			<th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($poblaciones as $poblacion)
        <tr>
            <td class="text-center">{{ $poblacion->codigo }}</td>
			<td class="text-center">{{ $poblacion->nominacion }}</td>
			<td class="text-center">{{ $poblacion->codigo_provincia }}</td>
			<td class="text-center">{{ $poblacion->nombre()->first()->nominacion }} </td>
          	<td class="text-center">{{ $poblacion->superficie }}</td>
		    <td class="text-center">{{ $poblacion->habitantes }}</td>
		    <td class="text-center">{{ $poblacion->gobierno }}</td>

            {!! Form::open(['route' => ['poblaciones.destroy', $poblacion->codigo], 'method' => 'DELETE']) !!}

            <td class="text-center">
                <button type="submit" class="btn btn-danger btn-xs">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <a href="{{ url('/poblaciones/'.$poblacion->codigo.'/edit') }}" class="btn btn-info btn-xs">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </a>
            </td>

            {!! Form::close() !!}

        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th class="text-center">Código</th>
		    <th class="text-center">Nominación</th>
			<th class="text-center">Código provincia</th>
			<th class="text-center">Provincia</th>     
			<th class="text-center">Superficie</th>
			<th class="text-center">Habitantes</th>
		    <th class="text-center">Gobierno</th>
			<th class="text-center">Acciones</th>
        </tr>
    </tfoot>
</table>