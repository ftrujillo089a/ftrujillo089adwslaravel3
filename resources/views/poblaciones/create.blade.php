@extends('poblaciones')

@section('content')
<h4 class="text-center">Alta de Población</h4>
{!! Form::open([ 'route' => 'poblaciones.store', 'method' => 'POST']) !!}
@include('poblaciones.partials.fields')

<button type="submit" class="btn btn-success btn-block">Guardar</button>

{!! Form::close() !!}

@endsection