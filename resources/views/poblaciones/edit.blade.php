@extends('poblaciones')

@section('content')

<h4 class="text-center">Editar Población: {{ $poblacion->codigo  }}</h4>

{!! Form::model($poblacion, [ 'route' => ['poblaciones.update', $poblacion], 'method' => 'PUT']) !!}

@include('poblaciones.partials.fields')

<button type="submit" class="btn btn-success btn-block">Guardar cambios</button>

{!! Form::close() !!}

@endsection

