
<div class="form-group">
    {!! Form::label('codigo', 'Código', ['for' => 'codigo'] ) !!}
    {!! Form::text('codigo', null , ['class' => 'form-control', 'codigo' => 'codigo', 'placeholder' => 'Escribe el código ...' ]  ) !!}

    {!! Form::label('nominacion', 'Nominación', ['for' => 'nominacion'] ) !!}
    {!! Form::text('nominacion', null , ['class' => 'form-control', 'nominacion' => 'nominacion', 'placeholder' => 'Escribe el nombre de la provincia...' ]  ) !!}
    {!! Form::label('superficie', 'Superficie', ['for' => 'superficie'] ) !!}
    {!! Form::text('superficie', null , ['class' => 'form-control', 'superficie' => 'superficie', 'placeholder' => 'Escribe la superficie...' ]  ) !!}
    
	{!! Form::label('habitantes', 'Habitantes', ['for' => 'habitantes'] ) !!}
    {!! Form::text('habitantes', null , ['class' => 'form-control', 'habitantes' => 'habitantes', 'placeholder' => 'Escribe los habitantes...' ]  ) !!}
    {!! Form::label('comunidad', 'Comunidad', ['for' => 'comunidad'] ) !!}
    {!! Form::text('comunidad', null , ['class' => 'form-control', 'comunidad' => 'comunidad', 'placeholder' => 'Escribe el nombre de la comunidad...' ]  ) !!}

</div>


