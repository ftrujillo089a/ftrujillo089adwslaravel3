<h1 class="text-primary">Control de Provincias</h1>

<table class="table table-bordered" id="MyTable">
    <thead>
        <tr>
            <th class="text-center">Código</th>
            <th class="text-center">Nominación</th>
			<th class="text-center">Superficie</th>
			<th class="text-center">Habitantes</th>
			<th class="text-center">Comunidad</th>
			
            <th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($provincias as $provincia)
        <tr>
            <td class="text-center">{{ $provincia->codigo }}</td>
            <td class="text-center">{{ $provincia->nominacion }}</td>
			<td class="text-center">{{ $provincia->superficie }}</td>
		    <td class="text-center">{{ $provincia->habitantes }}</td>
		    <td class="text-center">{{ $provincia->comunidad }}</td>

            {!! Form::open(['route' => ['provincias.destroy', $provincia->codigo], 'method' => 'DELETE']) !!}

            <td class="text-center">
                <button type="submit" class="btn btn-danger btn-xs">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <a href="{{ url('/provincias/'.$provincia->codigo.'/edit') }}" class="btn btn-info btn-xs">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </a>
            </td>

            {!! Form::close() !!}

        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th class="text-center">Código</th>
            <th class="text-center">Nominación</th>
			<th class="text-center">Superficie</th>
			<th class="text-center">Habitantes</th>
			<th class="text-center">Comunidad</th>
            <th class="text-center">Acciones</th>
        </tr>
    </tfoot>
</table>