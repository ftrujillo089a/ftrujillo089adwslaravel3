@extends('provincias')

@section('content')
<h4 class="text-center">Alta de Provincia</h4>
{!! Form::open([ 'route' => 'provincias.store', 'method' => 'POST']) !!}
@include('provincias.partials.fields')

<button type="submit" class="btn btn-success btn-block">Guardar</button>

{!! Form::close() !!}

@endsection