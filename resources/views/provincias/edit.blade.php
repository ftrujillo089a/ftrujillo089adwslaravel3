@extends('provincias')

@section('content')

<h4 class="text-center">Editar Provincia: {{ $provincia->codigo  }}</h4>

{!! Form::model($provincia, [ 'route' => ['provincias.update', $provincia], 'method' => 'PUT']) !!}

@include('provincias.partials.fields')

<button type="submit" class="btn btn-success btn-block">Guardar cambios</button>

{!! Form::close() !!}

@endsection

