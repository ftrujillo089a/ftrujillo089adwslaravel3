

@extends('provincias')

@section('content')

<a class="btn btn-success pull-right" href="{{ url('/provincias/create') }}" role="button">Nueva provincia</a>

@include('provincias.partials.table')

@endsection

