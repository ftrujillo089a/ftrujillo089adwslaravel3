<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPoblacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poblacion', function (Blueprint $table) {
            $table->integer('codigo');
            $table->integer('codigo_provincia');
            $table->string('nominacion',50);
            $table->integer('superficie');
            $table->integer('habitantes');
            $table->string('gobierno',50);
			$table->foreign('codigo_provincia')->references('codigo')->on('provincia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poblacion');
    }
}
