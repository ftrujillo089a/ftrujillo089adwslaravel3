<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hello', function () {
    return view('welcome');
});
 /* Control de usuarios */
Route::group(['middleware' => 'auth'], function() {

Route::get('/documenta', function () {
    return view('documentacion');
});

 Route::resource('provincias', 'ProvinciaController');
 Route::resource('poblaciones', 'PoblacionController');
});

Auth::routes();
Route::get('/home', 'HomeController@index');
