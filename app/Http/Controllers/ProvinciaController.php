<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;;
use Illuminate\Http\Request;
use App\Provincia;

class ProvinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provincias = Provincia::get();
        return view('provincias.index')->with('provincias', $provincias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('provincias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $provincia = new Provincia;

       /* $max = Provincia::get()->max('id');*/
	    $provincia->codigo = $request->input('codigo');
        $provincia->nominacion = $request->input('nominacion');
		$provincia->superficie = $request->input('superficie');
		$provincia->habitantes = $request->input('habitantes');
		$provincia->comunidad = $request->input('comunidad');
        $provincia->save();

        return redirect()->route('provincias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($codigo)
    {
        $provincia = Provincia::find($codigo);
        return view('provincias.edit')->with('provincia', $provincia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $codigo)
    {
        $provincia = Provincia::find($codigo);
        $provincia->codigo = $codigo;
        $provincia->nominacion = $request->input('nominacion');
		$provincia->superficie = $request->input('superficie');
		$provincia->habitantes = $request->input('habitantes');
		$provincia->comunidad = $request->input('comunidad');
        $provincia->save();
        return redirect()->route('provincias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo)
    {
        Provincia::destroy($codigo);
        return redirect()->route('provincias.index');
    }
}
