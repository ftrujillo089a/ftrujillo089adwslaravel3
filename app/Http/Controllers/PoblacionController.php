<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poblacion;

class PoblacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poblaciones = Poblacion::get();
        return view('poblaciones.index')->with('poblaciones', $poblaciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('poblaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $poblacion = new poblacion;

        $poblacion->codigo = $request->input('codigo');
		$poblacion->codigo_provincia = $request->input('codigo_provincia');
        $poblacion->nominacion = $request->input('nominacion');
		$poblacion->superficie = $request->input('superficie');
		$poblacion->habitantes = $request->input('habitantes');
		$poblacion->gobierno = $request->input('gobierno');
        $poblacion->save();

        return redirect()->route('poblaciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($codigo)
    {
       $poblacion = poblacion::find($codigo);
        return view('poblaciones.edit')->with('poblacion', $poblacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $codigo)
    {
        $poblacion = Poblacion::find($codigo);
        $poblacion->codigo = $codigo;
		$poblacion->codigo_provincia = $request->input('codigo_provincia');
        $poblacion->nominacion = $request->input('nominacion');
		$poblacion->superficie = $request->input('superficie');
		$poblacion->habitantes = $request->input('habitantes');
		$poblacion->gobierno = $request->input('gobierno');
        $poblacion->save();
        return redirect()->route('poblaciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $codigo
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo)
    {
        Poblacion::destroy($codigo);
        return redirect()->route('poblaciones.index');
    }
}
