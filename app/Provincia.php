<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends  Model
              {
    //
	 protected $table = 'provincia';
	 public $timestamps = false;
	 // necesario para que la clave no sea id
	 protected $primaryKey = 'codigo'; 
	 
	 //Una provincia tiene muchas poblaciones
	public function nombreProvincia(){
		return $this->hasMany('Poblacion');	
	}
}
