<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poblacion extends Model
{
    //
	 protected $table = 'poblacion';
	 // necesario para que la clave no sea id
	 protected $primaryKey = 'codigo';
	 public $timestamps = false;
	 
	//Una población pertenece a una provincia y sólo una
	public function nombre(){
		return $this->belongsTo('App\Provincia', 'codigo_provincia');	
	} 

}
